import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {RestApiService} from '../rest-api.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})


export class LoginComponent implements OnInit {
  details = {
    warehouseType : "",
    validationType: ""
  }
  validationType = [
    {
      "title": "Mandatory field is missing",
      "value": "Mandatory field is missing"
    },
    {
      "title": "Data field and type is incorrect",
      "value": "Data field and type is incorrect"
    },
    {
      "title": "Metric Value Mismatch",
      "value": "Metric Value Mismatch"
    },
    {
      "title": "Duplicates in Primary key/composite Key",
      "value": "Duplicates in Primary key/composite Key"
    }
  ]
  warehouseType = [
    {
      "title" : "Snowflake",
      "value" : "Snowflake"
    },
    {
      "title" : "AWS Redshift",
      "value" : "redshift"
    },
    {
      "title" : "MS Azure",
      "value" : "azure"
    },
    {
      "title" : "GCP Big Query",
      "value" : "gcp"
    }
  ]
  isValidated;
  table_data; 
  tableList;
  headers = ["Batch date", "Table Name", "Error Code", "Error Description", "Error Severity"]

  constructor(private router: Router, private restApi: RestApiService) { }

  ngOnInit() {
  }

  warehouseChanged(value) {
    if(value!== "Snowflake") {
      this.restApi.getTablesList(value).subscribe(res=> {
        console.log(res);
        this.tableList = res["data"];
      })
    }
  }

  explore() {
    this.isValidated = "true";
    let data = {
      "warehouse" : this.details.warehouseType
    }
    
    this.restApi.getExecutionSummary(data).subscribe(res => {
      this.table_data = res["data"];
    })
  }

}
