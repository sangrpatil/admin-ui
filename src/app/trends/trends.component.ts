import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {RestApiService} from '../rest-api.service'
import * as CanvasJS from '../../assets/externalJS/canvasjs';

@Component({
  selector: 'app-trends',
  templateUrl: './trends.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./trends.component.scss']
})
export class TrendsComponent implements OnInit {
  table1_headers = ["Error Description", "Error Count", "vs Prior"];
  table2_headers = ["Batch Date", "Target Table", "Metric Name", "SRC Value", "STG Value", "TGT Value", "Sync Indicator"]
  table1_data;
  table2_data;
  rollingPeriod = "Current Month";
  chart1Data = [];
  chart2Data = [];
  innerObj;
  tempDataPoints = [];
  columnChartColor = ["#749911", "gray", '#005da6', '#000'];
  periods = [
    {
      "title":"Current Month",
      "value": "Current Month"
     },
     {
       "title": "Last 3 Months",
       "value": "3"
     },
     {
       "title": "Last 6 Months",
       "value": "6"
     }
    ];

  constructor(private restApi: RestApiService) { }

  ngOnInit() {
      this.restApi.getReportingTableData1().subscribe(res=> {
        this.table1_data = res["data"];
      })
      this.restApi.getReportingTableData2().subscribe(res=> {
        this.table2_data = res["data"];
      })
      this.renderChart1();
  }

  periodChanged(value) {
    if(value == "Current Month") {
      this.renderChart1();
    } else {
      this.renderChart2();
    }
  }

  renderChart1() {
    this.restApi.getMonthlyErrorCount().subscribe(res=> {
      this.chart1Data = [];
      let count = 0;
      let that = this;
      for(let key in res["data"]) {
        let obj = {};
        for(let k in res["data"][key]) {
          let value = res["data"][key][k];
          this.innerObj = {};
          for(let i in value) {
            if(i == "date") {
              this.innerObj["label"] = value[i];
            } else {
              this.innerObj["y"] = Number(value[i]);
            }
          }
          that.tempDataPoints.push(this.innerObj);
        }
        obj = {
          type: "column",
          showInLegend: true,
          name: key,
          color: that.columnChartColor[count],
          dataPoints: that.tempDataPoints
        }
        that.chart1Data.push(obj);
        count++;
        that.tempDataPoints = [];
      }
      var chart = new CanvasJS.Chart("chart1", {
        animationEnabled: true,
        title:{
          text: "Error Count by Error Type"
        },
        axisY: {
          title: ""
        },
        legend: {
          cursor:"pointer",
          itemclick : toggleDataSeries
        },
        data: this.chart1Data
      });
      chart.render();
      
      function toggleDataSeries(e) {
        if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
          e.dataSeries.visible = false;
        }
        else {
          e.dataSeries.visible = true;
        }
        chart.render();
      }
    })
    
  }

  renderChart2() {
    this.restApi.getCumulativeErrorCount(this.rollingPeriod).subscribe(res=> {
      this.chart2Data = [];
      let that = this;
      for(let key in res["data"]) {
        that.chart2Data.push({y : res["data"][key]["count"], name: res["data"][key]["name"]})
      }
      var chart = new CanvasJS.Chart("chart1", {
        width: 700,
        height: 400,
        animationEnabled: true,
        title:{
          text: "Cumulative Error Count for " + this.rollingPeriod
        },
        legend:{
          cursor: "pointer"
        },
        data: [{
          type: "pie",
          showInLegend: true,
          toolTipContent: "{name}: <strong>{y}</strong>",
          indexLabel: "{name} - {y}",
          dataPoints: this.chart2Data
        }]
      });
      chart.render();
    })
  }
}
