import { Component, OnInit } from '@angular/core';
import { RestApiService } from '../rest-api.service'
import { Router } from '@angular/router';
import { MatSnackBar, MatSnackBarModule} from '@angular/material';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(private restApi: RestApiService, private router: Router, private snackBar: MatSnackBar) { }

  ngOnInit() {
  }
}
