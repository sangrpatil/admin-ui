import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RestApiService {
  public dateStart: number;
  public dateEnd: number;
  public keyword: string;

  constructor(private http: HttpClient) { }

  getExecutionSummary(param) {
    return this.http.get(environment.base_url + 'validate/summary-report?data_warehouse=' + param.warehouse);
  }

  getMonthlyErrorCount() {
    return this.http.get(environment.base_url + 'error/count-by-type');
  }

  getCumulativeErrorCount(param) {
    return this.http.get(environment.base_url + 'error/cumulative-count?rolling_period=' + param)
  }

  getTablesList(param) {
    return this.http.get('/assets/data/tableList.json');
    //return this.http.get(environment.base_url + 'filter/tables?data_warehouse=' + param)
  }

  getReportingTableData1() {
    return this.http.get(environment.base_url + 'error/current-vs-prior');
  }

  getReportingTableData2() {
    return this.http.get(environment.base_url + 'error/sync-indicator');
  }
}
